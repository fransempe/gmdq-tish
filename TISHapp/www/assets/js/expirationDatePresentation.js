function fillExpirationDatesItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['FECHAS_VENCIMIENTO'];
    var dataBindings =
    {
        '|RECURSO|': itemDataContent['RECURSO'],
        '|ANIO|': itemDataContent['ANIO'],
        '|CUOTA|': itemDataContent['CUOTA'],
        '|FEC_VTO1_CUOTA|': itemDataContent['FEC_VTO1_CUOTA'],
        '|FEC_VTO2_CUOTA|': itemDataContent['FEC_VTO2_CUOTA'],
        '|FEC_VTO_PRES_DJ|': itemDataContent['FEC_VTO_PRES_DJ']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function getExpirationDatePresentationGeneral ( successCallback, failCallback, workingDesignPopupLoginGMDQCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_Obtener_Fechas_Vencimientos_RFM/Obtener_Fechas_Vencimientos_RFM",
        "",
        successCallback,
        failCallback,
        workingDesignPopupLoginGMDQCallback
    );
}

function getExpirationDatePresentation ( pRecurso, pAnio, pCuota, successCallback, failCallback, workingDesignPopupLoginGMDQCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_Obtener_Fecha_Vencimiento_RFM/Obtener_Fecha_Vencimiento_RFM",
        "pRecurso="+pRecurso+"&pAnio="+pAnio+"&pCuota="+pCuota,
        successCallback,
        failCallback,
        workingDesignPopupLoginGMDQCallback
    );
}

function failGetAllExpirationDates()
{
    alert("Not data loaded in the data base!");
}
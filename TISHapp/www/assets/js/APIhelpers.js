function sendAjaxRequest(httpMethod, relativeURL, parameters, successHandler, failureHandler, workingFNHandler, formWithFiles)
{
    var url = BASE_URL + relativeURL;
    var data = null;

    if (httpMethod == 'POST')
        data = parameters;
    else if (parameters)
        url = url + '?' + parameters;

    var request = createRequest(url, httpMethod, data, formWithFiles);
    request.done(function (response)
    {
            var result = JSON.parse(response);
            switch(result.success)
            {
                case RESPONSE_SUCCESS:
                    if (successHandler)
                        successHandler(result.result_found, result.data);
                break;
                case RESPONSE_FAILURE:
                    if (failureHandler)
                        failureHandler(result);
                break;
                case GMDQ_WORKING:
                    console.error('Request Failed: ' + url + '\n' + response);
                    if (workingFNHandler)
                        workingFNHandler(result);
                break;
            }
    });

    request.fail(function (jqXHR, textStatus)
    {
        console.error("request failed: " + textStatus);
    });
}

function createRequest(url, httpMethod, data, formWithFiles)
{
    if (formWithFiles)
        return $.ajax({url: url, type: httpMethod, data: data, cache: false, contentType: false, processData: false});
    return $.ajax({url: url, type: httpMethod, data: data});
}

function workingDesignPopupFNCallback()
{
    showElement("divIframeView");
}
function loadPage(url, targetContainer)
{
    loadPageWithParameters(url, targetContainer, []);
}

function loadPageWithParameters(url, targetContainer, parameters)
{
    $.ajax(
        {
            url: url,
            async: false,
            success: function (data)
            {
                for (var key in parameters)
                    data = replaceAll(data, key, parameters[key]);

                $(targetContainer).append(data);
            },
            dataType: 'html'
        }
    );
}

function escapeRegExp(string)
{
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(string, find, replace)
{
    return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function replaceDataBindings(dataBindings, template)
{
    for (var key in dataBindings)
        template = replaceAll(template, key, dataBindings[key]);
    return template;
}

function showElement(elementId)
{
    document.getElementById(elementId).style.display = 'block';
}

function closeElement(elementId)
{
    document.getElementById(elementId).style.display = 'none';
}

function closeAllElement()
{
    closeElement("hoursTableTemplate");
    closeElement("employeesTableTemplate");
    closeElement("usersTableTemplate");
    closeElement("projectsTableTemplate");
}

function closeTablet(elementId)
{
    footerPosition(2);
    document.getElementById(elementId).style.display = 'none';
}

function footerPosition(footerId, position)
{
    if (position==1)
    {
        document.getElementById(footerId).style.position = 'relative';
    }
    if (position==2)
    {
        document.getElementById(footerId).style.position = 'absolute';
    }
}

function formatParameters(parameters)
{
    var params = '';
    for (var key in parameters)
    {
        var value = parameters[key];
        if (value)
            params += key + '=' + encodeURI(parameters[key]) + '&';
    }
    if (params)
        return params.substring(0, params.length - 1);
    return '';
}

Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});

function fillListWithTemplate(data, itemTemplate, fillHandler)
{
    return fillListWithTemplateWithKey('content', data, itemTemplate, fillHandler);
}

function fillListWithTemplateWithKey(jsonKey, data, itemTemplate, fillHandler)
{
    var itemList = "";

    for (var i = 0; i < (data[jsonKey].length); i++)
    {
        data[jsonKey][i]['position'] = i;
        itemList += fillHandler(data[jsonKey][i], itemTemplate);
    }
    return itemList;
}

function isNumberKey(evt) 
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
    {
        return false;
    } 
    else
    {
        if (evt.target.value.search(/\./) > -1 && charCode == 46) {
            return false;
        }
        return true;
    }
}

function getTemplate(templateName)
{
    var templateHtml = '';
    $.ajax(
        {
            url: './templates/' + templateName + '.html',
            async: false,
            success: function (data)
            {
                templateHtml = data;
            },
            dataType: 'html'
        }
    );
    return templateHtml;
}

function subStrings(text)
{
    var response = text;
    if (text.length > 10)
    {
        response = response.substring(0, 8) + '...';
    }
    return(response);
}

function validate(evt)
{
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}

function locationVars(elementRequired)
{
    var src = String( window.location.href ).split('?')[1];
    var vrs = src.split('&');

    for (var x = 0, c = vrs.length; x < c; x++)
    {
        if (vrs[x].indexOf(elementRequired) != -1)
        {
            return decodeURI( vrs[x].split('=')[1] );
            break;
        };
    };
};

function getQueryVariable(variable) {
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0; i < vars.length; i++) {
       var pair = vars[i].split("=");
       if(pair[0] == variable) {
           return pair[1];
       }
   }
   return false;
}

function emptyInputText(id)
{
    document.getElementById(id).value = '';
}

function emptyLabelText(id)
{
    document.getElementById(id).innerHTML = '';
}

function dataEntryNull(id, text)
{
    document.getElementById(id).style.color = '#006ebd';
    document.getElementById(id).innerHTML = text;
}

function fieldEmpty(id, text)
{
    document.getElementById(id).style.color = '#e80000';
    document.getElementById(id).innerHTML = text;
}

function dataEntryOk(id, text)
{
    document.getElementById(id).style.color = '#c8c8c8';
    document.getElementById(id).innerHTML = text;
}

function dataLoadedSuccessfully(id, text)
{
    document.getElementById(id).style.color = '#3A9A3A';
    document.getElementById(id).innerHTML = text;
}

function soloNumeros(e){
    var key = window.Event ? e.which : e.keyCode
    return (key >= 48 && key <= 57)
}

function formatDate(date)
{
    var init = 3;
    var end = 6;
    var mesString = date.substring(init, end);
    var mes;
    switch(mesString)
    {
        case "ENE":
            mes= '01';
            break;
        case "FEB":
            mes= '02';
            break;
        case "MAR":
            mes= '03';

            break;
        case "ABR":
            mes= '04';
            break;
        case "MAY":
            mes= '05';
            break;
        case "JUN":
            mes= '06';
            break;
        case "JUL":
            mes= '07';
            break;
        case "AGO":
            mes= '08';
            break;
        case "SEP":
            mes= '09';
            break;
        case "OCT":
            mes= '10';
            break;
        case "NOV":
            mes= '11';
            break;
        case "DIC":
            mes= '12';
            break;
    }
    var changeMes = date.replace(mesString, mes)
    var res = changeMes.replace("-", "/");
    var secondRes = res.replace("-", "/");
    return secondRes;
}

function changeMonthInWordsForNumber(monthInWords)
{
    var monthInNumbers;
    switch(monthInWords)
          {
            case "Enero":
                monthInNumbers = '01';
                  break;
            case "Febrero":
                monthInNumbers = '02';
                  break;
            case "Marzo":
                monthInNumbers = '03';
                  break;
            case "Abril":
                monthInNumbers = '04';
                  break;
            case "Mayo":
                monthInNumbers = '05';
                  break;
            case "Junio":
                monthInNumbers = '06';
                  break;
            case "Julio":
                monthInNumbers = '07';
                  break;
            case "Agosto":
                monthInNumbers = '08';
                  break;
            case "Septiembre":
                monthInNumbers = '09';
                  break;
            case "Octubre":
                monthInNumbers = '10';
                  break;
            case "Noviembre":
                monthInNumbers = '11';
                  break;
            case "Diciembre":
                monthInNumbers = '12';
                  break;
          } 
          return(monthInNumbers);
}

function formatDateCorrectForAdd(date){
   var  correctDate = date.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
   return correctDate;
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function formato_evt( evt ) 
{
    evt = evt.toString().replace(/$|,/g, '');
    if (isNaN(evt))
        evt = "0";

    sign = (evt == (evt = Math.abs(evt)));
    evt = Math.floor(evt * 100 + 0.50000000001);
    cents = evt % 100;
    evt = Math.floor(evt / 100).toString();

    if (cents < 10)
        cents = "0" + cents;

    for (var i = 0; i < Math.floor((evt.length - (1 + i)) / 3); i++)
        evt = evt.substring(0, evt.length - (4 * i + 3)) + '.' +

    evt.substring(evt.length - (4 * i + 3));
    return (((sign) ? '' : '-') + '$ ' + evt + ',' + cents)  
    //return (((sign) ? '' : '-') + evt + ',' + cents)
}


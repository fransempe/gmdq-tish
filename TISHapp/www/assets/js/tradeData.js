function fillTradeDataItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['DATOS_COMERCIO'];
    var dataBindings =
    {
        '|NRO_COMERCIO|': itemData['NRO_COMERCIO'],
        '|NOMBRE_COMERCIO|': itemData['NOMBRE_COMERCIO'],
        '|RUBRO|': itemData['RUBRO'],
        '|CALLE|': itemData['CALLE'],
        '|NROCALLE|': itemData['NROCALLE']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function getAllTradeData ( pNroComercio, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_Obtener_Datos_Comercio_RFM/Obtener_Datos_Comercio",
        "pNroComercio="+pNroComercio,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetAllTradeData()
{
    alert("Error: no se puede obtener datos de comercio")
}

function getResponseContribAsociadoComercio ( pNroComercio, pNroContrib, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_Comercio_Vinculado_RFM/Comercio_Vinculado_RFM",
        "pNroComercio="+pNroComercio+"&pNroContrib="+pNroContrib,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetResponseContribAsociadoComercio()
{
    alert("Error: no se puede obtener respuesta de comercio vinculado")
}

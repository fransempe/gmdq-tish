function fillWebUsersItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['WEB_USER_DATA'];
    var dataBindings =
    {
        '|ID_WEB_USER|': itemData['ID_WEB_USER']
        
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}


function addWebUsers( pDataManager, pDataTrade, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_create_user_web_RFM/create_user_web_RFM",
        "pDataManager="+pDataManager+"&pDataTrade="+pDataTrade,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetAllWebUsers()
{
    alert("Not data loaded in the data base!");
}

function addWebUsersCallback(result_found, data)
{
    alert('Usuario web agregado con éxito.' );
    clave = data['content'][0]['DATA_USER_WEB']['ADD_RESPONSE'];
    removeItemsForAddWebUsers(clave);
}

function failWebUsers()
{
    alert("NO se pudo dar de alta");
}
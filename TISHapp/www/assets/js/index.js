function fillDataIndexItem(itemData, itemTemplate)
{
    var itemDataContent = itemData['MUNICIPALIDAD'];
    var dataBindings =
    {
        '|MUNICIPALIDAD_DIRECCION|':itemDataContent['MUNICIPALIDAD_DIRECCION'],
        '|MUNICIPALIDAD_TELEFONO|': itemDataContent['MUNICIPALIDAD_TELEFONO'],
        '|MUNICIPALIDAD_MAIL|': itemDataContent['MUNICIPALIDAD_MAIL'],
        '|MUNICIPALIDAD_WEB|': itemDataContent['MUNICIPALIDAD_WEB']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}


function getDataIndex(successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_datos_municipalidad/ObtenerDatosMunicipalidad",
        "",
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetDataIndex()
{
    alert("No employees are found in database");
}



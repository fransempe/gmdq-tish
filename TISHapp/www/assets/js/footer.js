function fillDataFooterItem(itemData, itemTemplate)
{
    var itemDataContent = itemData['MUNICIPALIDAD'];
    var dataBindings =
    {
        '|MUNICIPALIDAD_WEB|':itemDataContent['MUNICIPALIDAD_WEB']
       /* '|MUNICIPALIDAD_NOMBRE|':itemDataContent['MUNICIPALIDAD_NOMBRE'],
        '|MUNICIPALIDAD_TELEFONO|': itemDataContent['MUNICIPALIDAD_TELEFONO'],
        '|MUNICIPALIDAD_MAIL|': itemDataContent['MUNICIPALIDAD_MAIL']*/
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}


function getDataFooter(successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_Obtener_Datos_Municipalidad_string_RFM/Obtener_Datos_Municipalidad",
        "",
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetDataFooter()
{
    alert("No data are found in database");
}



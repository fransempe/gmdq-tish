function fillManagerDataItem( itemData, itemTemplate )
{
    var itemDataManagerContent = itemData['MANAGER'];
    var itemDataTradesContent = itemData['TRADES'];
    var dataBindings =
    {
        '|NRO_COMERCIO|': itemDataTradesContent['NRO_COMERCIO'],
        '|RAZON_SOCIAL|': itemDataTradesContent['RAZON_SOCIAL'],
        '|CUIT|': itemDataManagerContent['CUIT'],
        '|CLAVE|': itemDataManagerContent['CLAVE'],
        '|TIPO_CLAVE|': itemDataManagerContent['TIPO_CLAVE'],
        '|DIRECCION|': itemDataManagerContent['DIRECCION'],
        '|CONTACTO|': itemDataManagerContent['CONTACTO'],
        '|TELEFONO|': itemDataManagerContent['TELEFONO'],
        '|EMAIL|': itemDataManagerContent['EMAIL'],
        '|EMAIL_EMPRESA|': itemDataManagerContent['EMAIL_EMPRESA'],
        '|NUMBER RESULT|': itemData['NUMBER RESULT']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function getDataCuit (pCuit, successCallback, failCallback, workingFNCallback ){
 sendAjaxRequest
    (
        "GET",
        "app_Obtener_Datos_Contribuyente_RFM/Obtener_Datos_Contribuyente_RFM",
        "pCuit="+pCuit,
        successCallback,
        failCallback,
        workingFNCallback
    );

}

function getManagerData ( pCuit, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_ObtainDataManager_RFM/ObtainDataManager_RFM",
        "pCuit="+pCuit,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetDataManager()
{
    alert("Not data loaded in the data base!");
}

function addFuelWebUsersCallback()
{
    alert('Usuario web agregado con éxito.' );
}

function failWebUsers()
{
    alert("NO se pudo dar de alta");
}
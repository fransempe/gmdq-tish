function fillRubroDataItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['DATOS_RUBROS'];
    var dataBindings =
    {
        '|NRO_COMERCIO|': itemDataContent['RUBRO']['NRO_COMERCIO'],
        '|RUBRO_NRO|': itemDataContent['RUBRO']['RUBRO_NRO'],
        '|RUBRO_ITEM|': itemDataContent['RUBRO']['RUBRO_ITEM'],
        '|RUBRO_DESCRIPCION|': itemDataContent['RUBRO']['RUBRO_DESCRIPCION'],
        '|RUBRO_ALICUOTA|': itemDataContent['RUBRO']['RUBRO_ALICUOTA'],
        '|RUBRO_MINIMO|': itemDataContent['RUBRO']['RUBRO_MINIMO'],
        '|NUMBER RESULT|': itemData['NUMBER RESULT']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function getAllRubroData ( pNroComercio, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_Obtener_Datos_Rubro_RFM/Obtener_Datos_Rubro_RFM",
        "pNroComercio="+pNroComercio,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetAllRubroData()
{
    alert("Rubros no encontrados");
}
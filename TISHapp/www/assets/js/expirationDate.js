function fillExpirationDatesItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['FECHAS_VENCIMIENTO'];
    var dataBindings =
    {
        '|RECURSO|': itemDataContent['RECURSO'],
        '|ANIO|': itemDataContent['ANIO'],
        '|CUOTA|': itemDataContent['CUOTA'],
        '|FEC_VTO1_CUOTA|': itemDataContent['FEC_VTO1_CUOTA'],
        '|FEC_VTO2_CUOTA|': itemDataContent['FEC_VTO2_CUOTA'],
        '|FEC_VTO_PRES_DJ|': itemDataContent['FEC_VTO_PRES_DJ']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function getAllExpirationDates ( noParammeters, successCallback, failCallback, workingDesignPopupLoginGMDQCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_Obtener_Fechas_Vencimientos_RFM/Obtener_Fechas_Vencimientos_RFM",
        "",
        successCallback,
        failCallback,
        workingDesignPopupLoginGMDQCallback
    );
}

function failGetAllExpirationDates()
{
    alert("Not data loaded in the data base!");
}

function addFechaVencimiento( pDataFechaVencimiento, successCallback, failCallback, workingFNCallback )
{

    sendAjaxRequest
    (
        "GET",
        "app_alta_fecha_vencimiento_RFM/alta_fecha_vencimiento_RFM",
        "pDataFechaVencimiento="+pDataFechaVencimiento,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failAddFechaVencimiento()
{
    alert("NO se pudo dar de alta");
}


function deleteFechaVencimiento( pDataFechaVencimiento, successCallback, failCallback, workingFNCallback )
{
    
    sendAjaxRequest
    (
        "GET",
        "app_borra_fecha_vencimiento_RFM/borra_fecha_vencimiento_RFM",
        "pDataFechaVencimiento="+pDataFechaVencimiento,
        successCallback,
        failCallback,
        workingFNCallback
    );
}



function failDeleteFechaVencimiento()
{
    alert("No se pudo borrar fecha de vencimiento");
}
function fillDDJJItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['DATOS_DDJJ'];
    var dataBindings =
    {
        '|NRO_COMERCIO|': itemDataContent['NRO_COMERCIO'],
        '|ANIO|': itemDataContent['ANIO'],
        '|CUOTA|': itemDataContent['CUOTA'],
        '|FECHA_PRES|': itemDataContent['FECHA_PRES'],
        '|MONTO_DECLARADO|': formato_evt(itemDataContent['MONTO_DECLARADO'])
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function fillInvoiceDDJJItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['DATA_INVOICE'];
    var dataBindings =
    {
        '|NROCOMP|': itemDataContent['NROCOMP'],
        '|FECHA_EMISION|': itemDataContent['FECHA_EMISION'],
        '|RECURSO|': itemDataContent['RECURSO'],
        '|ANIO|': itemDataContent['ANIO'],
        '|CUOTA|': itemDataContent['CUOTA'],
        '|CONCEPTO|': itemDataContent['CONCEPTO'],
        '|CODPLAN|': itemDataContent['CODPLAN'],
        '|IMPORIGENRENG|': itemDataContent['IMPORIGENRENG'],
        '|IMPTOTALRENG|': itemDataContent['IMPTOTALRENG'],
        '|FECHA_VENCIMIENTO_1|': itemDataContent['FECHA_VENCIMIENTO_1'],
        '|IMPORTE_TOTAL_ORIGEN1|': itemDataContent['IMPORTE_TOTAL_ORIGEN1'],
        '|IMPORTE_RECARGO_1|': itemDataContent['IMPORTE_RECARGO_1'],
        '|IMPORTE_TOTAL_1|': itemDataContent['IMPORTE_TOTAL_1'],
        '|FECHA_VENCIMIENTO_2|': itemDataContent['FECHA_VENCIMIENTO_2'],
        '|IMPORTE_TOTAL_ORIGEN2|': itemDataContent['IMPORTE_TOTAL_ORIGEN2'],
        '|IMPORTE_RECARGO_2|': itemDataContent['IMPORTE_RECARGO_2'],
        '|IMPORTE_TOTAL_2|': itemDataContent['IMPORTE_TOTAL_2'],
        '|FECHA_VENCIMIENTO_3|': itemDataContent['FECHA_VENCIMIENTO_3'],
        '|IMPORTE_TOTAL_ORIGEN3|': itemDataContent['IMPORTE_TOTAL_ORIGEN3'],
        '|IMPORTE_RECARGO_3|': itemDataContent['IMPORTE_RECARGO_3'],
        '|IMPORTE_TOTAL_3|': itemDataContent['IMPORTE_TOTAL_3'],
        '|STRINGCODBARRA|': itemDataContent['STRINGCODBARRA']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function getListDDJJ ( pNroComercio, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_Obtener_DDJJ_RFM/Obtener_DDJJ_RFM",
        "pNroComercio="+pNroComercio,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetListDDJJ()
{
    alert("Not data loaded in the data base!");
}

function existDDJJ ( pNroComercio, pAnio, pCuota, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_Existe_DDJJ_RFM/EXISTE_DDJJ_RFM",
        "pNroComercio="+pNroComercio+"&pAnio="+pAnio+"&pCuota="+pCuota,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failExistDDJJ()
{
    alert("Error al consultar si existe DDJJ!");
}


function addDDJJ ( pDataDDJJ, pFechaPresentacion, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_alta_DDJJ_RFM/alta_DDJJ_RFM",
        "pDataDDJJ="+pDataDDJJ+"&pFechaPresentacion="+pFechaPresentacion,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failAddDDJJ()
{
    alert("Error: no se puedo realizar la carga de DDJJ!");
}
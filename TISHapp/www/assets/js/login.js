var COOKIE_USER_ID = "L_USER";
var COOKIE_LOGIN = "gmdq7772017";
var REMEMBER_LOGIN = 0;

function fillDataUser(itemData, itemTemplate)
{
    var itemDataContent = itemData['MUNICIPALIDAD'];
    var dataBindings =
    {
        '|USER_FOUND|':itemDataContent['USER_FOUND']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function userLoginContribuyentes(userName, userPass, successCallback, failCallback, workingGMDQCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_Login_contribuyente_RFM/Login_contribuyente_RFM",
        formatParameters({'userName' : ''+userName+'', 'password' : ''+userPass+''}),
        successCallback,
        failCallback,
        workingGMDQCallback
    );
}

function failLoginUser( result_found, data )
{
    var loginResponse = data['message'];
    if ( loginResponse == "userName is Required!" )
    {
        showMessageFailLogin( 'failLoginUserCuit' );
    }
    if ( loginResponse == "password is Required!" )
    {
        showMessageFailLogin( 'failLoginUserPass' );
    }
}


function adminUserLogin(userName, userPass, successCallback, failCallback, workingGMDQCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_LoginRAFAM_boolean/LoginRAFAM",
        formatParameters({'userName' : ''+userName+'', 'password' : ''+userPass+''}),
        successCallback,
        failCallback,
        workingGMDQCallback
    );
}

function loginUserAdminCallback( result_found, data )
{
    var loginResponse = data['content'][0]['MUNICIPALIDAD']['LOGIN'];
    if (loginResponse == "true")
    {
        showSectionContent('views/admin_main_menu.html', 'content');
    }
    else
    {
         incorrectLoginAdmin();
    }
}

function failLoginAdminUser( result_found, data )
{
    var loginResponse = data['message'];
    if ( loginResponse == "userName is Required!" )
    {
        showMessageFailLogin( 'failLoginUserCuit' );
    }
    if ( loginResponse == "password is Required!" )
    {
        showMessageFailLogin( 'failLoginUserPass' );
    }
}



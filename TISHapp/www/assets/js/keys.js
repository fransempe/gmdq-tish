function fillDataKeyItem( itemData, itemTemplate )

{
    var itemDataContent = itemData['MUNICIPALIDAD'];
    var dataBindings =
    {
        '|KEY|': itemDataContent['KEY']
       
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function fillEditKey( itemData, itemTemplate )
{
    var itemDataContent = itemData['MUNICIPALIDAD'];
    var dataBindings =
    {
        '|KEY|': itemDataContent['KEY']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function getClearedKey (pCuit, successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_ClearKey_RFM/ClearKey_RFM",
        "pCUIT="+pCuit,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function editKey( pCuit, pKeyManager, pNew_KeyManager, successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_EditKey_RFM/EditKey_RFM",
        "pCUIT="+pCuit+"&pKeyManager="+pKeyManager+"&pNew_KeyManager="+pNew_KeyManager,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failClearKey()
{
    alert("Error al blanquear la clave.");
}

function addFuelWebUsersCallback()
{
    alert('Usuario web agregado con éxito.' );
}

function failEditKey()
{
    alert("Error no se pudo realizar el cambio de clave");
}


<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></html>

<?php


$NROCOMP = $_GET['NROCOMP'];
$FECHA_EMISION = $_GET['FECHA_EMISION'];

$RECURSO1 = $_GET['RECURSO1'];
$ANIO1 = $_GET['ANIO1'];
$CUOTA1 = $_GET['CUOTA1'];
$CONCEPTO1 = $_GET['CONCEPTO1'];
$CODPLAN1 = $_GET['CODPLAN1'];
$IMPORIGENRENG1 = $_GET['IMPORIGENRENG1'];
$IMPRECARGOSRENG1 = $_GET['IMPRECARGOSRENG1'];
$IMPTOTALRENG1 = $_GET['IMPTOTALRENG1'];

$RECURSO2 = $_GET['RECURSO2'];
$ANIO2 = $_GET['ANIO2'];
$CUOTA2 = $_GET['CUOTA2'];
$CONCEPTO2 = $_GET['CONCEPTO2'];
$CODPLAN2 = $_GET['CODPLAN2'];
$IMPORIGENRENG2 = $_GET['IMPORIGENRENG2'];
$IMPRECARGOSRENG2 = $_GET['IMPRECARGOSRENG2'];
$IMPTOTALRENG2 = $_GET['IMPTOTALRENG2'];

$FECHA_VENCIMIENTO_1 = $_GET['FECHA_VENCIMIENTO_1'];
$IMPORTE_TOTAL_ORIGEN1 = $_GET['IMPORTE_TOTAL_ORIGEN1'];
$IMPORTE_RECARGO_1 = $_GET['IMPORTE_RECARGO_1'];
$IMPORTE_TOTAL_1 = $_GET['IMPORTE_TOTAL_1'];
$FECHA_VENCIMIENTO_2 = $_GET['FECHA_VENCIMIENTO_2'];
$IMPORTE_TOTAL_ORIGEN2 = $_GET['IMPORTE_TOTAL_ORIGEN2'];;
$IMPORTE_RECARGO_2 = $_GET['IMPORTE_RECARGO_2'];
$IMPORTE_TOTAL_2 = $_GET['IMPORTE_TOTAL_2'];
$FECHA_VENCIMIENTO_3 = $_GET['FECHA_VENCIMIENTO_3'];
$IMPORTE_TOTAL_ORIGEN3 = $_GET['IMPORTE_TOTAL_ORIGEN3'];
$IMPORTE_RECARGO_3 = $_GET['IMPORTE_RECARGO_3'];
$IMPORTE_TOTAL_3 = $_GET['IMPORTE_TOTAL_3'];
$STRINGCODBARRA = $_GET['STRINGCODBARRA'];




//VARIABLES
$MUNICIPIO = "LINCOLN";


$NROCOMERCIO = $_GET['NROCOMERCIO'];
$TITULAR =  $_GET['razonSocial'];
$NOMBREFANTASIA =  $_GET['NOMBRE_FANTASIA'];
$DOMICILIO = $_GET['DIRECCION'];

$TOTAL = $IMPORTE_TOTAL_1;

$COMPROBANTES1 = array($RECURSO1, $ANIO1, $CUOTA1, $CONCEPTO1, $IMPORIGENRENG1,$IMPORTE_RECARGO_1, $IMPTOTALRENG1);
$COMPROBANTES2 = array($RECURSO2, $ANIO2, $CUOTA2, $CONCEPTO2, $IMPORIGENRENG2,$IMPORTE_RECARGO_1, $IMPTOTALRENG2);
$TOTALORIGEN = $IMPORTE_TOTAL_1;
$TOTALRECARGO = $IMPORTE_RECARGO_1;
$CODIGO_BARRA1 = $STRINGCODBARRA;



	function cabecera($pdf, $pTITULAR,$pNOMBREFANTASIA,$pDOMICILIO,$pNROCOMERCIO,$pUBICACION){
			$pdf->Line(10,37,200,37); //linea horizontal arriba
			$pdf->Line(10,37,10,62); //linea vertical izq
			$pdf->Line(200,37,200,62); //linea vertical derecha
			$pdf->Line(10,62,200,62);	//linea horizontal abajo
			$cadena = utf8_decode("Titular: " .$pTITULAR);			
			$pdf->Cell(20,6,$cadena,0,0,'L');
			$cadena = utf8_decode("Tipo de Cuenta: COMERCIO");			
			$pdf->Cell(150,6,$cadena,0,0,'R');			
			$pdf->Ln();
			$cadena = utf8_decode("Nombre fantasía: " .$pNOMBREFANTASIA);			
			$pdf->Cell(20,6,$cadena,0,0,'L');
			$pdf->Ln();
			$cadena = utf8_decode("Domicilio: " .$pDOMICILIO);			
			$pdf->Cell(20,6,$cadena,0,0,'L');
			$cadena = utf8_decode("Nro. de Comercio: $pNROCOMERCIO");			
			$pdf->Cell(150,6,$cadena,0,0,'R');
			$pdf->Ln();			

	}


	function cuerpo($pdf,$pCOMPROBANTES1,$pCOMPROBANTES2,$pTOTAL,$pVENCIMIENTO,$pNROCOMP){

			$pdf->Line(10,66,200,66); //linea horizontal arriba
			$pdf->Line(10,66,10,190); //linea vertical izq
			$pdf->Line(200,190,200,66); //linea vertical derecha
			$pdf->Line(10,190,200,190);	//linea horizontal abajo
			$cadena = utf8_decode("Nro. Comprobante: " .$pNROCOMP);			
			$pdf->Cell(20,25,$cadena,0,0,'A');
			$pdf->Line(10,74,200,74);	//linea horizontal abajo
			$pdf->Ln(20);
			$cadena = utf8_decode("Recurso");			
			$pdf->Cell(20,0,$cadena,0,0,'L');
			$cadena = utf8_decode("Año");			
			$pdf->Cell(12,0,$cadena,0,0,'L');
			$cadena = utf8_decode("Cuota");			
			$pdf->Cell(15,0,$cadena,0,0,'L');
			$cadena = utf8_decode("Concepto");			
			$pdf->Cell(50,0,$cadena,0,0,'L');
			$cadena = utf8_decode("Imp. Origen");			
			$pdf->Cell(35,0,$cadena,0,0,'L');
			$cadena = utf8_decode("Imp. Recargo");			
			$pdf->Cell(35,0,$cadena,0,0,'L');
			$cadena = utf8_decode("Imp. Total");			
			$pdf->Cell(35,0,$cadena,0,0,'L');
			$pdf->Ln(6);

////////////////////COMPROBANTES/////////////////////

			$pdf->SetFont('Arial','',7);
			$cadena = utf8_decode($pCOMPROBANTES1{0});			
			$pdf->Cell(20,0,$cadena,0,0,'L');
			$cadena = utf8_decode($pCOMPROBANTES1{1});			
			$pdf->Cell(12,0,$cadena,0,0,'A');
			$cadena = utf8_decode($pCOMPROBANTES1{2});			
			$pdf->Cell(15,0,$cadena,0,0,'L');
			$cadena = utf8_decode($pCOMPROBANTES1{3});			
			$pdf->Cell(50,0,$cadena,0,0,'L');
			$cadena = utf8_decode($pCOMPROBANTES1{4});			
			$pdf->Cell(20,0,$cadena,0,0,'R');
			$cadena = utf8_decode($pCOMPROBANTES1{5});			
			$pdf->Cell(38,0,$cadena,0,0,'R');
			$cadena = utf8_decode($pCOMPROBANTES1{6});			
			$pdf->Cell(28,0,$cadena,0,0,'R');
			$pdf->Ln();
			$cadena = utf8_decode($pCOMPROBANTES2{0});			
			$pdf->Cell(20,10,$cadena,10,0,'L');
			$cadena = utf8_decode($pCOMPROBANTES2{1});			
			$pdf->Cell(12,10,$cadena,10,0,'A');
			$cadena = utf8_decode($pCOMPROBANTES2{2});			
			$pdf->Cell(15,10,$cadena,10,0,'L');
			$cadena = utf8_decode($pCOMPROBANTES2{3});			
			$pdf->Cell(50,10,$cadena,10,0,'L');
			$cadena = utf8_decode($pCOMPROBANTES2{4});			
			$pdf->Cell(20,10,$cadena,10,0,'R');
			$cadena = utf8_decode($pCOMPROBANTES2{5});			
			$pdf->Cell(38,10,$cadena,10,0,'R');
			$cadena = utf8_decode($pCOMPROBANTES2{6});			
			$pdf->Cell(28,10,$cadena,10,0,'R');
			$pdf->Ln();


			//TOTAL
			$pdf->SetFont('Arial','',14);
			$cadena = utf8_decode("TOTAL: ");
        	$pdf->SetY(160);
        	$pdf->SetX(155);			
			$pdf->Cell(10,0,$cadena,0,0,'R');
			$pdf->Cell(12,0,$pTOTAL,0,0,'A');		
        	$pdf->SetY(70);
        	$pdf->SetX(160);
			$cadena = utf8_decode("Fecha vencimiento: $FECHA_VENCIMIENTO_1");
			$pdf->Cell(10,0,$cadena,0,0,'R');
			$pdf->Cell(12,0,$pVENCIMIENTO,0,0,'A');
			$pdf->SetFont('Arial','',10);
			$pdf->SetY(188);
			$pdf->SetX(175);
			$pdf->Cell(180,0,"Contribuyente",0,0,'A');
			
	}

	//////////////////TALON DE PAGO/////////////////////
	function talon($pdf,$pMUNICIPIO,$pNROCOMERCIO,$pVENCIMIENTO,$pTOTALORIGEN,$pTOTALRECARGO,$pTOTAL,$pCODIGOBARRA1,$pCODIGOBARRA2,$pNROCOMP){
			$pdf->SetFont('Arial','',10);

			//1er cuadro
			$pdf->Line(10,220,110,220); //linea horizontal arriba
			$pdf->Line(10,220,10,280); //linea vertical izq
			$pdf->Line(110,280,110,220); //linea vertical derecha
			$pdf->Line(10,280,110,280);	//linea horizontal abajo

			//2do cuadro
			$pdf->Line(115,220,200,220); //linea horizontal arriba
			$pdf->Line(200,220,200,280); //linea vertical der
			$pdf->Line(115,280,115,220); //linea vertical izq
			$pdf->Line(115,280,200,280);	//linea horizontal abajo

        	$pdf->SetY(212);
        	$pdf->SetX(9);
        	$cadena = utf8_decode("Municipalidad de $pMUNICIPIO");
			$pdf->Cell(180,0,$cadena,0,0,'A');
			$pdf->SetY(212);
        	$pdf->SetX(114);
        	$cadena = utf8_decode("Municipalidad de $pMUNICIPIO");
			$pdf->Cell(180,0,$cadena,0,0,'A');
			
			$pdf->SetY(217);
        	$pdf->SetX(9);
        	$cadena = utf8_decode("Comprobante de Pago");
			$pdf->Cell(180,0,$cadena,0,0,'A');
			$pdf->SetY(217);
        	$pdf->SetX(114);
        	$cadena = utf8_decode("Comprobante de Pago");
			$pdf->Cell(180,0,$cadena,0,0,'A');

			$pdf->SetY(225);
			$pdf->SetX(11);
			$cadena = utf8_decode("Cuenta: C- $pNROCOMERCIO");
			$pdf->Cell(180,0,$cadena,0,0,'A');

			$pdf->SetY(225);
			$pdf->SetX(116);
			$cadena = utf8_decode("Cuenta: C- $pNROCOMERCIO");
			$pdf->Cell(180,0,$cadena,0,0,'A');

			$pdf->SetY(225);
			$pdf->SetX(50);
			$cadena = utf8_decode("Nº Comp.: $pNROCOMP");
			$pdf->Cell(180,0,$cadena,0,0,'A');

			$pdf->SetY(225);
			$pdf->SetX(146);
			$cadena = utf8_decode("Nº Comp.: $pNROCOMP");
			$pdf->Cell(180,0,$cadena,0,0,'A');


			$pdf->SetY(240);
			$pdf->SetX(11);
			$cadena = utf8_decode("Fecha de Vencimiento:  $pVENCIMIENTO");
			$pdf->Cell(180,0,$cadena,0,0,'A');

			$pdf->SetY(240);
			$pdf->SetX(117);
			$cadena = utf8_decode("Fecha de Vencimiento:  $pVENCIMIENTO");
			$pdf->Cell(180,0,$cadena,0,0,'A');

			$pdf->SetY(250);
			$pdf->SetX(11);
			$cadena = utf8_decode("Origen");
			$pdf->Cell(180,0,$cadena,0,0,'A');
			$pdf->SetY(256);
			$pdf->SetX(13);
			$pdf->Cell(180,0,$pTOTALORIGEN,0,0,'A');
			
			$pdf->SetY(250);
			$pdf->SetX(50);
			$cadena = utf8_decode("Recargos");
			$pdf->Cell(180,0,$cadena,0,0,'A');
			$pdf->SetY(256);
			$pdf->SetX(50);
			$pdf->Cell(180,0,$pTOTALRECARGO,0,0,'A');

			$pdf->SetY(250);
			$pdf->SetX(92);
			$cadena = utf8_decode("Total");
			$pdf->Cell(180,0,$cadena,0,0,'A');
			$pdf->SetY(256);
			$pdf->SetX(80);
			$pdf->Cell(180,0,$pTOTAL,0,0,'A');


			$pdf->SetY(250);
			$pdf->SetX(117);
			$cadena = utf8_decode("Origen");
			$pdf->Cell(180,0,$cadena,0,0,'A');
			$pdf->SetY(256);
			$pdf->SetX(119);
			$pdf->Cell(180,0,$pTOTALORIGEN,0,0,'A');
			
			$pdf->SetY(250);
			$pdf->SetX(147);
			$cadena = utf8_decode("Recargos");
			$pdf->Cell(180,0,$cadena,0,0,'A');
			$pdf->SetY(256);
			$pdf->SetX(149);
			$pdf->Cell(180,0,$pTOTALRECARGO,0,0,'A');

			$pdf->SetY(250);
			$pdf->SetX(187);
			$cadena = utf8_decode("Total");
			$pdf->Cell(180,0,$cadena,0,0,'A');
			$pdf->SetY(256);
			$pdf->SetX(175);
			$pdf->Cell(180,0,$pTOTAL,0,0,'A');

			codigo_barra($pdf,$pCODIGOBARRA1,$pCODIGOBARRA2);

			$pdf->SetY(276.9);
			$pdf->SetX(87);
			$pdf->Cell(180,0,"Municipalidad",0,0,'A');
			$pdf->SetY(276.9);
			$pdf->SetX(187);
			$pdf->Cell(180,0,"Banco",0,0,'A');

	}


/////////////////////////FUNCION CODIGO DE BARRAS////////////////////////////
	function codigo_barra($pdf,$pCODIGOBARRA1,$pCODIGOBARRA2){

			$pdf->SetFont('Arial','',8);
			$pdf->SetY(274);
			$pdf->SetX(17);


            $pdf->i25(12, 192, $pCODIGOBARRA1);
			//$pdf->Code39(120, 265, $pCODIGOBARRA2);


			$pdf->Cell(180,0,$COD,0,0,'A');
			$pdf->SetY(274);
			$pdf->SetX(140);
			//$pdf->Cell(100,0,$pCODIGOBARRA2,0,0,'A');

	}



	$fechanow=strftime( "%d-%m-%Y");
	require('fpdf.php');

	$pdf = new FPDF('P','mm','A4');
	$pdf->SetFont('Arial','',10);
	$pdf->SetTopMargin(30);
	$pdf->SetLeftMargin(12);
	$pdf->SetRightMargin(1);
	$pdf->AddPage();
	$pdf->Image('../images/logo-small.png',10,13,40);
	$cadena = utf8_decode("Municipalidad de $MUNICIPIO");
			$pdf->Cell(190,5,$cadena,0,0,'R');

			$pdf->SetFont('Arial','B',10);
			$pdf->SetFillColor(255,255,255);
			$pdf->Ln(10);
			
			cabecera($pdf,$TITULAR,$NOMBREFANTASIA,$DOMICILIO,$NROCOMERCIO,$UBICACION);

			cuerpo($pdf,$COMPROBANTES1,$COMPROBANTES2,$TOTAL,$FECHA_VENCIMIENTO_1, $NROCOMP);

			talon($pdf,$MUNICIPIO,$NROCOMERCIO,$FECHA_VENCIMIENTO_1,$TOTALORIGEN,$TOTALRECARGO,$TOTAL,$CODIGO_BARRA1,$CODIGO_BARRA2,$NROCOMP);

	$pdf->Ln();$Hoja++;
	ob_end_clean();
 	$modo="I"; 
    $nombre_archivo="ComprobanteDePago".$NROCOMERCIO.''.trim($fechanow).".pdf"; 
    $pdf->Output($nombre_archivo,$modo);  
//$pdf->Output();
?>


<?php
//VARIABLES
$MUNICIPIO = "LINCOLN";
$TOTAL_RESULTS = $_GET['totalResults'];
$TOTAL_TRADES = $_GET['totalTrades'];

$TITULAR =  $_GET['razonSocial'];
$NOMBREFANTASIA =  $_GET['NOMBRE_FANTASIA'];
$DOMICILIO = $_GET['DIRECCION']; 
$CUIT = $_GET['CUIT'];
$NROCOMERCIO = $_GET['NROCOMERCIO'];
$LOCALIDAD ="";
$pPERIODO = $_GET['CUOTA1'];
$pANIO = $_GET['ANIO1'];
$fechanow = $_GET['FECHA_EMISION'];

$MES = array();
$CODIGO = array();
$DESCRIPCION = array();
$MONTO_INGRESOS = array();
$MONTO_DEDUCCION = array();
$MONTO_IMPONIBLE = array();
$ALICUOTA = array();
$MINIMO = array();
$TASA_DETERMINADA_MEDIA= array();
//die($TOTAL_RESULTS);
$f = 1;
for($I=1;$I<=$TOTAL_RESULTS;$I++)
{
	$f = $f+1;
	/*FIRST MONTH*/

	$firstMonth = $_GET['firstMonth'.$I];
	$MES[$I] = $firstMonth;

	$firstMonthRubroNro = $_GET['firstMonthRubroNro'.$I];
	$CODIGO[$I]= $firstMonthRubroNro;

	$firstMonthDescr = $_GET['firstMonthDescr'.$I];
	$DESCRIPCION[$I] = $firstMonthDescr;

	$firstMonthTotalMontoIngresos = $_GET['firstMonthTotalMontoIngresos'.$I];
	$MONTO_INGRESOS[$I] = $firstMonthTotalMontoIngresos;

	$firstMonthTotalMontoDeducciones = $_GET['firstMonthTotalMontoDeducciones'.$I];
	$MONTO_DEDUCCION[$I] = $firstMonthTotalMontoDeducciones;

    $firstMonthMontoImponible = $_GET['firstMonthMontoImponible'.$I];
	$MONTO_IMPONIBLE[$I] = $firstMonthMontoImponible;

    $firstMonthAlicuota = $_GET['firstMonthAlicuota'.$I];
	$ALICUOTA[$I] = $firstMonthAlicuota;

    $firstMonthMinimo = $_GET['firstMonthMinimo'.$I];
	$MINIMO[$I] = $firstMonthMinimo;

	$firstMonthTasaPagar = $_GET['firstMonthTasaPagar'.$I];
	$TASA_DETERMINADA_MEDIA[$I] = $firstMonthTasaPagar;

    /*SECOND MONTH*/

	$secondMonth = $_GET['secondMonth'.$I];
	$MES[$I+$f] = $secondMonth;

	$secondMonthRubroNro = $_GET['secondMonthRubroNro'.$I];
	$CODIGO[$I+$f]= $secondMonthRubroNro;

	$secondMonthDescr = $_GET['secondMonthDescr'.$I];
	$DESCRIPCION[$I+$f] = $secondMonthDescr;

	$secondMonthTotalMontoIngresos = $_GET['secondMonthTotalMontoIngresos'.$I];
	$MONTO_INGRESOS[$I+$f] = $secondMonthTotalMontoIngresos;

	$secondMonthTotalMontoDeducciones = $_GET['secondMonthTotalMontoDeducciones'.$I];
	$MONTO_DEDUCCION[$I+$f] = $secondMonthTotalMontoDeducciones;

    $secondMonthMontoImponible = $_GET['secondMonthMontoImponible'.$I];
	$MONTO_IMPONIBLE[$I+$f] = $secondMonthMontoImponible;

    $secondMonthAlicuota = $_GET['secondMonthAlicuota'.$I];
	$ALICUOTA[$I+$f] = $secondMonthAlicuota;

    $secondMonthMinimo = $_GET['secondMonthMinimo'.$I];
	$MINIMO[$I+$f] = $secondMonthMinimo;

	$secondMonthTasaPagar = $_GET['secondMonthTasaPagar'.$I];
	$TASA_DETERMINADA_MEDIA[$I+$f] = $secondMonthTasaPagar;


}
//die();

$TOTAL = $_GET['IMPORTE_TOTAL_1'];
//die($TOTAL);

	$fechanow=strftime( "%d-%m-%Y");
	require('fpdf.php');
	$pdf=new FPDF('P','mm','A4');
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTopMargin(30);
	$pdf->SetLeftMargin(12);
	$pdf->SetRightMargin(1);
	$pdf->AddPage();


	function encabezado($pdf,$pTITULAR,$pRAZONSOCIAL,$pNOMBREFANTASIA,$pDIRECCION,$pNROCOMERCIO,$pCUIT,$pLOCALIDAD)
	{

		$cadena = utf8_decode("Titular: $pTITULAR");
		$pdf->Cell(92,5,$cadena,1,0,'A','True');
		$cadena = utf8_decode("Nº COMERCIO: $pNROCOMERCIO");
		$pdf->Cell(92,5,$cadena,1,0,'A','True');
		$pdf->Ln();
		$cadena = utf8_decode("Razón Social: $pRAZONSOCIAL");
		$pdf->Cell(92,5,$cadena,1,0,'A','True');
		$cadena = utf8_decode("CUIT: $pCUIT");
		$pdf->Cell(92,5,$cadena,1,0,'A','True');
		$pdf->Ln();
		$cadena = utf8_decode("Nombre fantasía: $pNOMBREFANTASIA");
		$pdf->Cell(92,5,$cadena,1,0,'A','True');
		//$cadena = utf8_decode("LOCALIDAD: $pLOCALIDAD");
		$pdf->Cell(92,5,$cadena,1,0,'A','True');
		$pdf->Ln();
		$cadena = utf8_decode("Ubicación: $pDIRECCION");
		$pdf->Cell(92,5,$cadena,1,0,'A','True');
		$pdf->Cell(92,5,"",1,0,'A','True');

    }

	function titulos_tabla( $pdf, $pfechanow, $pPERIODO, $pANIO )
	{
				$cadena = utf8_decode("Período: ".$pPERIODO." Año: ".$pANIO."                                                                                         Fecha de Presentación: $pfechanow");
				$pdf->Cell(184,5,$cadena,1,0,'A','True');
				$pdf->Ln();
				$pdf->Ln();		
				$pdf->SetFont('Arial','B',7);
				$cadena = utf8_decode("Actividades");			
				$pdf->Cell(75,5,$cadena,1,0,'C','True');
				$cadena = utf8_decode("");			
				$pdf->Cell(109,5,$cadena,1,0,'A','True');
				$pdf->Ln();	
				$cadena = utf8_decode("Mes");			
				$pdf->Cell(8,5,$cadena,1,0,'C','True');
				$cadena = utf8_decode("Código");			
				$pdf->Cell(15,5,$cadena,1,0,'C','True');
				$cadena = utf8_decode("Descripción");	
				$pdf->Cell(52,5,$cadena,1,0,'A','True');
				$cadena = utf8_decode("Monto Ing."); // |Monto Deducción |Monto Imponible |Alicuota |Mínimo |Tasa Det. Media");	
				$pdf->Cell(21,5,$cadena,1,0,'A','True');
				$cadena = utf8_decode("Monto Deduc.");
				$pdf->Cell(19,5,$cadena,1,0,'A','True');
				$cadena = utf8_decode("Monto Imp.");
				$pdf->Cell(20,5,$cadena,1,0,'A','True');
				$cadena = utf8_decode("Alicuota");
				$pdf->Cell(12,5,$cadena,1,0,'A','True');
				$cadena = utf8_decode("Mínimo");
				$pdf->Cell(17,5,$cadena,1,0,'C','True');
				$cadena = utf8_decode("Tasa Det.Media");
				$pdf->Cell(20,5,$cadena,1,0,'A','True');
			}	

	function cuerpo($pdf, $pMES, $pCODIGO, $pTOTAL_RESULTS,$pDESCRIPCION,$pMONTO_INGRESOS,$pMONTO_DEDUCCION,$pMONTO_IMPONIBLE,$pALICUOTA,
				 $pMINIMO, $pTASA_DETERMINADA_MEDIA, $pTOTAL){

			$f=1;
			for($I=1;$I<=$pTOTAL_RESULTS;$I++){
				$f=$f+1;
				$cadena = utf8_decode($pMES{$I});
				$pdf->Cell(8,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pCODIGO{$I});
				$pdf->Cell(15,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pDESCRIPCION{$I});
				$pdf->Cell(52,5,$cadena,1,0,'A','True');
				$cadena = utf8_decode($pMONTO_INGRESOS{$I});
				$pdf->Cell(21,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pMONTO_DEDUCCION{$I});
				$pdf->Cell(19,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pMONTO_IMPONIBLE{$I});
				$pdf->Cell(20,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pALICUOTA{$I});
				$pdf->Cell(12,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pMINIMO{$I});
				$pdf->Cell(17,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pTASA_DETERMINADA_MEDIA{$I});
				$pdf->Cell(20,5,$cadena,1,0,'R','True');
							$pdf->Ln();	
				$cadena = utf8_decode($pMES{$I+$f});
				$pdf->Cell(8,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pCODIGO{$I+$f});
				$pdf->Cell(15,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pDESCRIPCION{$I+$f});
				$pdf->Cell(52,5,$cadena,1,0,'A','True');
				$cadena = utf8_decode($pMONTO_INGRESOS{$I+$f});
				$pdf->Cell(21,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pMONTO_DEDUCCION{$I+$f});
				$pdf->Cell(19,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pMONTO_IMPONIBLE{$I+$f});
				$pdf->Cell(20,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pALICUOTA{$I+$f});
				$pdf->Cell(12,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pMINIMO{$I+$f});
				$pdf->Cell(17,5,$cadena,1,0,'R','True');
				$cadena = utf8_decode($pTASA_DETERMINADA_MEDIA{$I+$f});
				$pdf->Cell(20,5,$cadena,1,0,'R','True');
							$pdf->Ln();	

				}

	}		


			$pdf->Image('../images/logo-small.png',10,10,40);
			$pdf->SetFont('Arial','B',10);
			/*$cadena = utf8_decode("Fecha de Presentación: ");
			$pdf->Cell(190,5,$cadena.$fechanow,0,0,'R');*/
			$pdf->SetFont('Arial','B',12);
			$pdf->Ln();			
			$cadena = utf8_decode("Constancia de presentación de la Declaración Jurada");		
			$pdf->Cell(145,10,$cadena,0,0,'R');
			$pdf->Ln();
			$cadena = utf8_decode("de la Tasa por Inspección de Seg. e Higiene");		
			$pdf->Cell(135,10,$cadena,0,0,'R');
			//$cadena = utf8_decode("Municipalidad de $MUNICIPIO");
			//$pdf->Cell(115,10,$cadena,0,0,'R');
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->SetFillColor(255,255,255);
			$pdf->Ln();

			encabezado($pdf,$TITULAR,$RAZONSOCIAL,$NOMBREFANTASIA,$DOMICILIO,$NROCOMERCIO,$CUIT,$LOCALIDAD);
			$pdf->Ln();
			$pdf->Ln();
			titulos_tabla( $pdf, $fechanow, $pPERIODO, $pANIO );
			$pdf->Ln();

			//SE CREAN 2 CUERPOS PORQUE SON 2 RENGLONES DE PERIODOS

				cuerpo($pdf, $MES, $CODIGO, $TOTAL_RESULTS, $DESCRIPCION,$MONTO_INGRESOS,$MONTO_DEDUCCION,$MONTO_IMPONIBLE,$ALICUOTA,
				 $MINIMO, $TASA_DETERMINADA_MEDIA, $TOTAL);

				$pdf->Ln();
			

			//TOTAL
			$pdf->SetFont('Arial','B',14);
			$pdf->Cell(184,8,"TOTAL: ".$TOTAL,1,0,'R','True');
			$pdf->SetFont('Arial','B',10);

	$pdf->Ln();$Hoja++;
	ob_end_clean();
 	$modo="I"; 
    $nombre_archivo="ConstanciaDePresentacionDDJJ".$CUIT.''.trim($fechanow).".pdf"; 
    $pdf->Output($nombre_archivo,$modo);  
//$pdf->Output();
    
?>



<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></html>

<?php
//VARIABLES
$BLANQUEO = $_GET['blanqueo'];
$TOTAL_TRADES = $_GET['totalTrades'];
$MUNICIPIO = $_GET['municipio'];
$RAZON_SOCIAL = $_GET['razonSocial'];
$RESPONSABLE = $_GET['responsable'];
$DIRECCION = $_GET['direccion']; 
$TELEFONO = $_GET['telefono']; 
$EMAIL = $_GET['email'];
$EMAIL_EMPRESA = $_GET['email_empresa'];
$CUIT = $_GET['cuit'];
$CLAVE = $_GET['clave'];
$number_commerce_send_array = array();
$name_commerce_send_array = array();


for($I=1;$I<=$TOTAL_TRADES;$I++)
{
	$number_commerce_send = $_GET['number_commerce_send'.$I];
	//die($number_commerce_send );
	$number_commerce_send_array[$I] = $number_commerce_send;


	$name_commerce_send = $_GET['name_commerce_send'.$I];
	//die($name_commerce_send );
	$name_commerce_send_array[$I]= $name_commerce_send;
}



	$fechanow=strftime( "%d-%m-%Y");
	require('fpdf.php');
	$pdf=new FPDF('P','mm','A4');
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTopMargin(30);
	$pdf->SetLeftMargin(12);
	$pdf->SetRightMargin(1);
	$pdf->AddPage();


function comercios( $pdf, $TOTAL_TRADES, $number_commerce_send_array, $name_commerce_send_array)
	{
				$pdf->Ln();
				$pdf->Ln();
				$pdf->SetFont('Arial','B',8);
				$cadena =  utf8_decode("Nº Comercio");
				$pdf->Cell(20,5,$cadena,1,0,'A');
				$pdf->Cell(92,5,"Nombre Comercio",1,0,'A');
				
				for($I=0;$I<count($TOTAL_TRADES);$I++){
					$pdf->Ln();
					$I=$I+1;
					$pdf->Cell(20,5,$number_commerce_send_array{$I},1,0,'A');
					$cadena = utf8_decode($name_commerce_send_array{$I});
					$pdf->Cell(92,5,$cadena,1,0,'A');
				}


	}

	function cuerpo($pdf,$pRAZON_SOCIAL,$pRESPONSABLE,$pDIRECCION,$pTELEFONO,$pEMAIL,$pEMAIL_EMPRESA,$pCUIT,$pCLAVE){
				$pdf->Ln();
				$pdf->SetFont('Arial','B',8);
				$cadena =  utf8_decode("Razón Social: $pRAZON_SOCIAL");
				$pdf->Cell(92,5,$cadena,1,0,'A');
				$pdf->Cell(92,5,"CUIT: ".$pCUIT,1,0,'A');
				$pdf->Ln();
				$cadena =  utf8_decode("Responsable: $pRESPONSABLE");
				$pdf->Cell(92,5,$cadena,1,0,'A');
				$pdf->Cell(92,5,"CLAVE: ".$pCLAVE,1,0,'A');
				$pdf->Ln();				
				$cadena =  utf8_decode("Dirección: $pDIRECCION");
				$pdf->Cell(92,5,$cadena,1,0,'A');
				$pdf->Ln();				
				$cadena =  utf8_decode("Teléfono: $pTELEFONO");
				$pdf->Cell(92,5,$cadena,1,0,'A');
				$pdf->Ln();				
				$cadena =  utf8_decode("E-Mail: $pEMAIL");
				$pdf->Cell(92,5,$cadena,1,0,'A');
				$pdf->Ln();				
				$cadena =  utf8_decode("E-Mail Empresa: $pEMAIL_EMPRESA");
				$pdf->Cell(92,5,$cadena,1,0,'A');
			}	


			$pdf->Image('../images/logo-small.png',10,10,40);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(180,5,"Fecha: ".$fechanow,0,0,'R');
			$pdf->SetFont('Arial','B',12);
			$pdf->Ln();			
			$cadena = utf8_decode("CONSTANCIA DE CREACIÓN DE USUARIO WEB");		
			$pdf->Cell(140,10,$cadena,0,0,'R');
			$pdf->Ln();
			$cadena = utf8_decode("Municipalidad de $MUNICIPIO");
			$pdf->Cell(115,10,$cadena,0,0,'R');
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->SetFillColor(255,255,255);
			$pdf->Ln();
			$pdf->Cell(92,5,"Datos de Usuario WEB",1,0,'C','True');
			$pdf->Cell(92,5,"Datos para el ingreso WEB",1,0,'C','True');

			cuerpo($pdf,$RAZON_SOCIAL,$RESPONSABLE,$DIRECCION,$TELEFONO,$EMAIL,$EMAIL_EMPRESA,$CUIT,$CLAVE);

			comercios( $pdf, $TOTAL_TRADES, $number_commerce_send_array, $name_commerce_send_array);
			
			


			$pdf->Ln();
			$pdf->Ln();
			$cadena = utf8_decode("Este nuevo servicio permitirá a los contribuyentes que así lo deseen, presentar sus declaraciones juradas, sin necesidad de concurrir al");
			$pdf->Cell(140,5,$cadena,0,0,'A','True');
			$pdf->Ln();
			$cadena = utf8_decode("palacio municipal. El servicio es gratuito y de simple operación, cualquier contribuyente puede acceder al mismo. Para poder realizar sus ");
			$pdf->Cell(140,5,$cadena,0,0,'A','True');
			$pdf->Ln();			
			$cadena = utf8_decode("consultas ingrese a la página del municipio y luego acceda al link llamado Sistema TISH.");
			$pdf->Cell(140,5,$cadena,0,0,'A','True');
			$pdf->Ln();		
			$pdf->Ln();		
			$pdf->Ln();		
			$pdf->Ln();				
			$cadena = utf8_decode("Municipalidad de $MUNICIPIO");
			$pdf->Cell(10,5,$cadena,0,0,'A','True');	
	$pdf->Ln();$Hoja++;
	ob_end_clean();
	    
	 $modo="I"; 
    $nombre_archivo="ConstanciaDeClaveProvisoria".$CUIT.''.trim($fechanow).".pdf"; 
    $pdf->Output($nombre_archivo,$modo);  
//$pdf->Output();
?>

